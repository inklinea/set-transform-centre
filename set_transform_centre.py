#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Set Transform Centre - An Inkscape 1.2+ Extension
# Try to set the Inkscape inkscape:transform-center in absolute units
##############################################################################

import inkex

def get_attributes(self):
    for att in dir(self):
        try:
            inkex.errormsg((att, getattr(self, att)))
        except:
            None

class SetTransformCentre(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--absolute_x", type=float, dest="absolute_x", default=0)
        pars.add_argument("--absolute_y", type=float, dest="absolute_y", default=0)

    def effect(self):

        selection_list = self.svg.selected

        if not 0 < len(selection_list) < 2:
            inkex.errormsg('Please select one object')
            return

        my_object = selection_list[0]

        rc_x = my_object.get('inkscape:transform-center-x')
        rc_y = my_object.get('inkscape:transform-center-y')

        my_object_mid_x = my_object.bounding_box().center.x
        my_object_mid_y = my_object.bounding_box().center.y

        my_object_width = my_object.bounding_box().width
        my_object_height = my_object.bounding_box().height

        absolute_x = self.options.absolute_x
        absolute_y = self.options.absolute_y

        # lets compensate for possible layer transform ( translate only )

        point_x, point_y = my_object.getparent().composed_transform().apply_to_point([my_object_mid_x, my_object_mid_y])

        my_object.set('inkscape:transform-center-x', absolute_x - point_x)
        my_object.set('inkscape:transform-center-y', -absolute_y + point_y)

        # inkex.errormsg(point)

if __name__ == '__main__':
    SetTransformCentre().run()


